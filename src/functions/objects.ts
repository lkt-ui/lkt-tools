import {ILktObject} from '../interfaces/ILktObject';

export const cloneObject = (obj: object) => {
  if (typeof obj === 'object') {
    return Object.assign(Object.create(Object.getPrototypeOf(obj)), obj);
  }
  return {};
};

export const sortObject = (obj: ILktObject) => {
  // ES10 Solution
  return Object.fromEntries(Object.entries(obj).sort(([, a], [, b]) => a - b));
};

export const fetchInObject = (
  obj: ILktObject,
  key: string
): object | undefined => {
  const args = key.split('.');
  const argsLength = args.length;
  let c = 0;
  let t = obj;

  // Parse config data and fetch attribute
  while (typeof t[args[c]] !== 'undefined') {
    t = t[args[c]];
    ++c;
  }

  // If not found...
  if (c < argsLength) {
    return undefined;
  }

  return t;
};

export const mergeObjects = (obj1: object, obj2: object): object => {
  return { ...obj1, ...obj2 };
};

export const safeMergeObjects = (
  obj1: ILktObject,
  obj2: ILktObject
): object => {
  const keys1 = Object.keys(obj1),
    keys2 = Object.keys(obj2),
    keys = keys1.filter((value) => keys2.includes(value));

  const r: ILktObject = {};
  keys.forEach((z) => {
    return (r[z] = obj1[z] ?? obj2[z]);
  });

  return r;
};

export const deleteObjectKeys = (obj: ILktObject, keys: string[]): object => {
  const objKeys = Object.keys(obj);
  keys.forEach((key) => {
    if (objKeys.includes(key)) {
      delete obj[key];
    }
  });

  return obj;
};

export const objectToArray = (t: object): any[] => {
  return Object.keys(t).map(function (e) {
    // @ts-ignore
    return t[e];
  });
};
