import { ILktObject } from '../interfaces/ILktObject';

export const getSlots = (
  $slots: object,
  slotStartingPattern?: string
): ILktObject => {
  const r: ILktObject = {};
  let haystack: ILktObject = {};
  if ($slots) {
    haystack = Object.assign(haystack, $slots);
  }

  if (slotStartingPattern) {
    for (const k in haystack) {
      if (k.indexOf(slotStartingPattern) === 0) {
        r[k.replace(slotStartingPattern, '')] = haystack[k];
      }
    }
    return r;
  }

  for (const k in haystack) {
    r[k] = haystack[k];
  }
  return r;
};

export const slotProvided = ($vm: any, name: string) => {
  return !!$vm.$slots[name];
};
