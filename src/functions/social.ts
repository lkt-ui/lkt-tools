import {htmlEntities} from "./strings";
import {isUndefined} from "./types";

// @ts-ignore
export const getMailUrl = (email: string = undefined, subject:string = undefined, body:string = undefined): string => {
    const r = new URL(`mailto:${  email}`);

    if (!isUndefined(subject)) {
        r.searchParams.append('subject', subject);
    }

    if (!isUndefined(body)) {
        r.searchParams.append('body', body);
    }

    return r.href;
}

// @ts-ignore
export const getFacebookUrl = (url:string = undefined): string => {
    if (isUndefined(url)) {
        url = window.location.href;
    }
    const r = new URL('https://www.facebook.com/sharer/sharer.php');
    r.searchParams.append('u', url);
    return r.href;
}

// @ts-ignore
export const getTwitterUrl = (url:string = undefined, via:string = undefined, hashtags:any[] = []): string => {
    if (isUndefined(url)) {
        url = window.location.href;
    }
    const r = new URL('https://twitter.com/intent/tweet');
    r.searchParams.append('url', url);

    if (!isUndefined(via)){
        r.searchParams.append('via', via);
    }

    if (hashtags.length > 0){
        r.searchParams.append('hashtags', hashtags.join(' '));
    }
    return r.href;
}

// @ts-ignore
export const getGooglePlusUrl = (url:string = undefined): string => {
    if (isUndefined(url)) {
        url = window.location.href;
    }
    const r = new URL('https://plus.google.com/share');
    r.searchParams.append('url', url);
    return r.href;
}

// @ts-ignore
export const getLinkedInUrl = (url:string = undefined, title:string = undefined, summary:string = undefined, source:string = undefined): string => {

    if (isUndefined(url)) {
        url = window.location.href;
    }
    const r = new URL('https://www.linkedin.com/shareArticle');
    r.searchParams.append('mini', 'true');
    r.searchParams.append('url', htmlEntities(url));

    if (!isUndefined(title)){
        r.searchParams.append('title', htmlEntities(title));
    }

    if (!isUndefined(summary)){
        r.searchParams.append('summary', htmlEntities(summary));
    }

    if (!isUndefined(source)){
        r.searchParams.append('source', htmlEntities(source));
    }
    return r.href;
}

// @ts-ignore
export const getPinterestUrl = (url:string = undefined, media:string = undefined, description:string = undefined): string => {

    if (isUndefined(url)) {
        url = window.location.href;
    }
    const r = new URL('//es.pinterest.com/pin/create/button/');
    r.searchParams.append('url', url);

    if (!isUndefined(media)){
        r.searchParams.append('media', media);
    }

    if (!isUndefined(description)){
        r.searchParams.append('description', description);
    }

    return r.href;
}

// @ts-ignore
export const getWhatsAppUrl = (url:string = undefined, text:string = ''): string => {

    if (isUndefined(url)) {
        url = window.location.href;
    }

    text = `${htmlEntities(text)  } ${  url}`;

    let r = new URL('whatsapp://send');
    r.searchParams.append('text', text);
    return r.href;
}