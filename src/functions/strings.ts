import { ILktObject } from '../interfaces/ILktObject';
import { isString } from './types';

export const kebabCaseToCamelCase = (str = ''): string => {
  return str.replace(/-([a-z])/g, function (g) {
    return g[1].toUpperCase();
  });
};

export const trim = (t: string, e: string = '') => {
  let r,
    n = 0,
    o = 0;
  for (
    t += '',
      e
        ? ((e += ''), (r = e.replace(/([[\]().?/*{}+$^:])/g, '$1')))
        : (r = ' \n\r	\f            ​\u2028\u2029　'),
      n = t.length,
      o = 0;
    n > o;
    o++
  )
    if (r.indexOf(t.charAt(o)) === -1) {
      t = t.substring(o);
      break;
    }
  for (n = t.length, o = n - 1; o >= 0; o--)
    if (r.indexOf(t.charAt(o)) === -1) {
      t = t.substring(0, o + 1);
      break;
    }
  return r.indexOf(t.charAt(0)) === -1 ? t : '';
};

export const ucfirst = (t: string): string => {
  t += '';
  const e = t.charAt(0).toUpperCase();
  return e + t.substring(1);
};

export const replaceAll = (
  target: string,
  search: string,
  replacement: string
) => {
  return target.replace(new RegExp(search, 'g'), replacement);
};

export const replaceSingleWhiteSpaces = (
  target: string,
  replacement: string
) => {
  return target.replace(/\s/g, replacement);
};

export const stripTags = (input: string, allowed: string) => {
  if (input === null) {
    input = '';
  }
  // making sure the allowed arg is a string containing only tags in lowercase (<a><b><c>)
  allowed = (
    `${allowed || ''}`.toLowerCase().match(/<[a-z][a-z0-9]*>/g) || []
  ).join('');

  const tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi;
  const commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;

  return input.replace(commentsAndPhpTags, '').replace(tags, function ($0, $1) {
    return allowed.indexOf(`<${$1.toLowerCase()}>`) > -1 ? $0 : '';
  });
};

export const numberFormat = (t: any, e: any, r: any, n: any) => {
  t = `${t}`.replace(/[^0-9+\-Ee.]/g, '');
  const o = isFinite(+t) ? +t : 0,
    i = isFinite(+e) ? Math.abs(e) : 0,
    u = typeof n === 'undefined' ? ',' : n,
    a = typeof r === 'undefined' ? '.' : r,
    f = function (t: any, e: any) {
      const r = Math.pow(10, e);
      return `${(Math.round(t * r) / r).toFixed(e)}`;
    };
  return (
    // @ts-ignore
    (s = (i ? f(o, i) : `${Math.round(o)}`).split('.')),
    // @ts-ignore
    s[0].length > 3 && (s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, u)),
    // @ts-ignore
    (s[1] || '').length < i &&
      // @ts-ignore
      ((s[1] = s[1] || ''), (s[1] += new Array(i - s[1].length + 1).join('0'))),
    // @ts-ignore
    s.join(a)
  );
};

export const utf8Encode = (string: string) => {
  string = string.replace(/\r\n/g, '\n');
  let utftext = '';

  for (let n = 0; n < string.length; n++) {
    const c = string.charCodeAt(n);

    if (c < 128) {
      utftext += String.fromCharCode(c);
    } else if (c > 127 && c < 2048) {
      utftext += String.fromCharCode((c >> 6) | 192);
      utftext += String.fromCharCode((c & 63) | 128);
    } else {
      utftext += String.fromCharCode((c >> 12) | 224);
      utftext += String.fromCharCode(((c >> 6) & 63) | 128);
      utftext += String.fromCharCode((c & 63) | 128);
    }
  }

  return utftext;
};

export const utf8Decode = (utftext: string) => {
  let string = '',
    i = 0,
    c = 0,
    c2 = 0,
    c3 = 0;

  while (i < utftext.length) {
    c = utftext.charCodeAt(i);

    if (c < 128) {
      string += String.fromCharCode(c);
      i++;
    } else if (c > 191 && c < 224) {
      c2 = utftext.charCodeAt(i + 1);
      string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
      i += 2;
    } else {
      c2 = utftext.charCodeAt(i + 1);
      c3 = utftext.charCodeAt(i + 2);
      string += String.fromCharCode(
        ((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63)
      );
      i += 3;
    }
  }

  return string;
};

export const extractFillData = (
  str = '',
  replacements = {},
  leftSeparator = ':',
  rightSeparator = ''
) => {
  const r = [];
  for (const k in replacements) {
    // eslint-disable-next-line no-prototype-builtins
    if (replacements.hasOwnProperty(k)) {
      if (str.indexOf(leftSeparator + k + rightSeparator) > -1) {
        r.push(k);
      }
    }
  }
  return r;
};

export const fill = (
  str: string = '',
  replacements: ILktObject = {},
  leftSeparator: string = ':',
  rightSeparator: string = ''
) => {
  for (const k in replacements) {
    // eslint-disable-next-line no-prototype-builtins
    if (replacements.hasOwnProperty(k)) {
      // @ts-ignore
      str = str.replace(leftSeparator + k + rightSeparator, replacements[k]);
    }
  }
  return str;
};

export const generateRandomString = (length: number = 10): string => {
  let result = '';
  const characters =
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789',
    charactersLength = characters.length;

  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }

  return result;
};

export const htmlEntities = (str: string): string => {
  return String(str)
    .replace(/&/g, '&amp;')
    .replace(/</g, '&lt;')
    .replace(/>/g, '&gt;')
    .replace(/"/g, '&quot;');
};

export const toString = (datum: any): string => String(datum);

export const isFilled = (str: any): boolean => {
  if (!isString(str)) {
    str = toString(str);
  }

  return !!str;
};

export const cloneString = (str: string) => ` ${str}`.slice(1);

export function lpad(n: string | number, c: number, char: string) {
  n = String(n);
  while (n.length < c) {
    n = char + n;
  }
  return n;
}
