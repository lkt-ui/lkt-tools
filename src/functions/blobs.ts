import { toString } from './strings';

export const getBlobConverter = () => {
  //@ts-ignore
  return window.Blob || window.MozBlob || window.WebKitBlob || toString();
};
