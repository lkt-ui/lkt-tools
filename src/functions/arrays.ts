/**
 *
 * @param needle
 * @param haystack
 * @param strict
 */
export function inArray<T>(needle: unknown, haystack: T[], strict: boolean = true): boolean {
    return haystack.includes(needle as T);
    let key = '';
    if (strict) {
        for (key in haystack) {
            if (haystack[key] === needle) {
                return true;
            }
        }
    } else {
        for (key in haystack) {
            if (haystack[key] == needle) {
                return true;
            }
        }
    }
    return false;
}

/**
 *
 * @param start
 * @param end
 */
export const createArrayFromRange = (start: number, end: number): number[] => {
    const array = [];

    for (let i = start; i < end; i++) {
        array.push(i);
    }
    return array;
};

/**
 *
 * @param myArr
 * @param prop
 */
export function rmArrayDuplicates<T>(myArr: T[], prop: any): T[] {
    return myArr.filter((obj, pos, arr) => {
        // @ts-ignore
        return arr.map((mapObj) => mapObj[prop]).indexOf(obj[prop]) === pos;
    });
}

/**
 *
 * @param myArr
 * @param value
 */
export function rmArrayValue<T>(myArr: T[], value: any): T[] {
    const index = myArr.indexOf(value);
    if (index >= 0) {
        myArr.splice(index, 1);
    }
    return myArr;
}

/**
 *
 * @param arr
 * @param prop
 */
export const getArrayUniqueKeyValues = (arr: any[], prop: any): any[] => {
    return (
        arr
            .map((e) => e[prop])

            // store the keys of the unique objects
            .map((e, i, final) => final.indexOf(e) === i && i)

            // eliminate the dead keys & store unique objects
            // @ts-ignore
            .filter((e) => arr[e])
            // @ts-ignore
            .map((e) => arr[e])
    );
};

/**
 *
 * @param arr
 */
export const cloneArray = (arr: any[]): any[] => {
    return [...arr];
};
