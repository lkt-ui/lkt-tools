export const isBase64String = (src = ''): boolean => {
  return typeof src === 'string' && src !== '' && src.indexOf('data:image/') !== -1;
};
