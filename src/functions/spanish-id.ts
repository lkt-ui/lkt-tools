import { CIF_REGEX, DNI_LETTERS, DNI_REGEX, NIE_REGEX } from '../constants';
import { ValidSpanishIdResponse } from '../interfaces/ValidSpanishIdResponse';
import { SpanishIdType } from '../types/Types';

export const validateSpanishID = (str: string) => {
  // Ensure upper case and remove whitespace
  str = str.toUpperCase().replace(/\s/, '');

  let valid = false;
  const type = getSpanishIdType(str);

  switch (type) {
    case 'dni':
      valid = validDNI(str);
      break;
    case 'nie':
      valid = validNIE(str);
      break;
    case 'cif':
      valid = validCIF(str);
      break;
  }

  return {
    type,
    valid,
  };
};

export const getSpanishIdType = (str: string): SpanishIdType | undefined => {
  if (str.match(DNI_REGEX)) {
    return 'dni';
  }
  if (str.match(CIF_REGEX)) {
    return 'cif';
  }
  if (str.match(NIE_REGEX)) {
    return 'nie';
  }
};

export const validDNI = (dni: string): boolean => {
  return dni.charAt(8) === DNI_LETTERS.charAt(parseInt(dni, 10) % 23);
};

export const validNIE = (nie: string): boolean => {
  // Change the initial letter for the corresponding number and validate as DNI
  const nie_prefix: string = nie.charAt(0);
  let r: number = 0;

  switch (nie_prefix) {
    case 'X':
      r = 0;
      break;
    case 'Y':
      r = 1;
      break;
    case 'Z':
      r = 2;
      break;
  }

  return validDNI(r + nie.substring(1));
};

export const validCIF = (cif: string): boolean => {
  const match: RegExpMatchArray | null = cif.match(CIF_REGEX);

  if (!match) {
    return false;
  }

  const letter = match[1],
    number = match[2],
    control = match[3];

  let even_sum: number = 0;
  let odd_sum: number = 0;
  let n: number;

  for (let i = 0; i < number.length; i++) {
    n = parseInt(number[i], 10);

    // Odd positions (Even index equals to odd position. i=0 equals first position)
    if (i % 2 === 0) {
      // Odd positions are multiplied first.
      n *= 2;

      // If the multiplication is bigger than 10 we need to adjust
      odd_sum += n < 10 ? n : n - 9;

      // Even positions
      // Just sum them
    } else {
      even_sum += n;
    }
  }

  const sum = parseFloat((even_sum + odd_sum).toString().substring(-1));

  const control_digit = 10 - sum,
    control_letter = 'JABCDEFGHI'.substring(control_digit, 1);

  // Control must be a digit
  if (letter.match(/[ABEH]/)) {
    //@ts-ignore
    return control == control_digit;
  }

  // Control must be a letter
  if (letter.match(/[KPQS]/)) {
    return control == control_letter;
  }

  // Can be either
  //@ts-ignore
  return control == control_digit || control == control_letter;
};

export const isValidSpanishID = (value: string): ValidSpanishIdResponse => {
  return validateSpanishID(value);
};
