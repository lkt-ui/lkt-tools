import { DATE_TEXTS, ONE_YEAR_IN_SECONDS } from '../constants';
import { ILktObject } from '../interfaces/ILktObject';
import { lpad } from './strings';

export const date = (format: string, timestamp: any = undefined) => {
  let jsdate: any;
  let f: ILktObject = {};
  // Keep this here (works, but for code commented-out below for file size reasons)
  // let tal= [];

  // trailing backslash -> (dropped)
  // a backslash followed by any character (including backslash) -> the character
  // empty string -> empty string
  const formatChr = /\\?(.?)/gi;
  const formatChrCb = function (t: any, s: any) {
    return f[t] ? f[t]() : s;
  };
  f = {
    // Day
    d() {
      // Day of month w/leading 0; 01..31
      return lpad(f.j(), 2, '0');
    },
    D() {
      // Shorthand day name; Mon...Sun
      return f.l().slice(0, 3);
    },
    j() {
      // Day of month; 1..31
      return jsdate.getDate();
    },
    l() {
      // Full day name; Monday...Sunday
      return `${DATE_TEXTS[f.w()]}day`;
    },
    N() {
      // ISO-8601 day of week; 1[Mon]..7[Sun]
      return f.w() || 7;
    },
    S() {
      // Ordinal suffix for day of month; st, nd, rd, th
      const j = f.j();
      let i = j % 10;
      if (i <= 3 && parseInt(String((j % 100) / 10), 10) === 1) {
        i = 0;
      }
      return ['st', 'nd', 'rd'][i - 1] || 'th';
    },
    w() {
      // Day of week; 0[Sun]..6[Sat]
      return jsdate.getDay();
    },
    z() {
      // Day of year; 0..365
      const a = new Date(f.Y(), f.n() - 1, f.j());
      const b = new Date(f.Y(), 0, 1);
      // @ts-ignore
      return Math.round((a - b) / 864e5);
    },

    // Week
    W() {
      // ISO-8601 week number
      const a = new Date(f.Y(), f.n() - 1, f.j() - f.N() + 3);
      const b = new Date(a.getFullYear(), 0, 4);
      // @ts-ignore
      return lpad(1 + Math.round((a - b) / 864e5 / 7), 2, '0');
    },

    // Month
    F() {
      // Full month name; January...December
      return DATE_TEXTS[6 + f.n()];
    },
    m() {
      // Month w/leading 0; 01...12
      return lpad(f.n(), 2, '0');
    },
    M() {
      // Shorthand month name; Jan...Dec
      return f.F().slice(0, 3);
    },
    n() {
      // Month; 1...12
      return jsdate.getMonth() + 1;
    },
    t() {
      // Days in month; 28...31
      return new Date(f.Y(), f.n(), 0).getDate();
    },

    // Year
    L() {
      // Is leap year?; 0 or 1
      const j = f.Y();
      // @ts-ignore
      return ((j % 4 === 0) & (j % 100 !== 0)) | (j % 400 === 0);
    },
    o() {
      // ISO-8601 year
      const n = f.n();
      const W = f.W();
      const Y = f.Y();
      return Y + (n === 12 && W < 9 ? 1 : n === 1 && W > 9 ? -1 : 0);
    },
    Y() {
      // Full year; e.g. 1980...2010
      return jsdate.getFullYear();
    },
    y() {
      // Last two digits of year; 00...99
      return f.Y().toString().slice(-2);
    },

    // Time
    a() {
      // am or pm
      return jsdate.getHours() > 11 ? 'pm' : 'am';
    },
    A() {
      // AM or PM
      return f.a().toUpperCase();
    },
    B() {
      // Swatch Internet time; 000..999
      const H = jsdate.getUTCHours() * 36e2;
      // Hours
      const i = jsdate.getUTCMinutes() * 60;
      // Minutes
      // Seconds
      const s = jsdate.getUTCSeconds();
      return lpad(Math.floor((H + i + s + 36e2) / 86.4) % 1e3, 3, '0');
    },
    g() {
      // 12-Hours; 1..12
      return f.G() % 12 || 12;
    },
    G() {
      // 24-Hours; 0..23
      return jsdate.getHours();
    },
    h() {
      // 12-Hours w/leading 0; 01..12
      return lpad(f.g(), 2, '0');
    },
    H() {
      // 24-Hours w/leading 0; 00..23
      return lpad(f.G(), 2, '0');
    },
    i() {
      // Minutes w/leading 0; 00..59
      return lpad(jsdate.getMinutes(), 2, '0');
    },
    s() {
      // Seconds w/leading 0; 00..59
      return lpad(jsdate.getSeconds(), 2, '0');
    },
    u() {
      // Microseconds; 000000-999000
      return lpad(jsdate.getMilliseconds() * 1000, 6, '0');
    },

    // Timezone
    e() {
      // Timezone identifier; e.g. Atlantic/Azores, ...
      // The following works, but requires inclusion of the very large
      // timezone_abbreviations_list() function.
      /*              return that.date_default_timezone_get();
       */
      const msg =
        'Not supported (see source code of date() for timezone on how to add support)';
      throw new Error(msg);
    },
    I() {
      // DST observed?; 0 or 1
      // Compares Jan 1 minus Jan 1 UTC to Jul 1 minus Jul 1 UTC.
      // If they are not equal, then DST is observed.
      const a = new Date(f.Y(), 0);
      // Jan 1
      const c = Date.UTC(f.Y(), 0);
      // Jan 1 UTC
      const b = new Date(f.Y(), 6);
      // Jul 1
      // Jul 1 UTC
      const d = Date.UTC(f.Y(), 6);
      // @ts-ignore
      return a - c !== b - d ? 1 : 0;
    },
    O() {
      // Difference to GMT in hour format; e.g. +0200
      const tzo = jsdate.getTimezoneOffset();
      const a = Math.abs(tzo);
      return (
        (tzo > 0 ? '-' : '+') +
        lpad(Math.floor(a / 60) * 100 + (a % 60), 4, '0')
      );
    },
    P() {
      // Difference to GMT w/colon; e.g. +02:00
      const O = f.O();
      return `${O.substr(0, 3)}:${O.substr(3, 2)}`;
    },
    T() {
      // The following works, but requires inclusion of the very
      // large timezone_abbreviations_list() function.
      return 'UTC';
    },
    Z() {
      // Timezone offset in seconds (-43200...50400)
      return -jsdate.getTimezoneOffset() * 60;
    },

    // Full Date/Time
    c() {
      // ISO-8601 date.
      return 'Y-m-d\\TH:i:sP'.replace(formatChr, formatChrCb);
    },
    r() {
      // RFC 2822
      return 'D, d M Y H:i:s O'.replace(formatChr, formatChrCb);
    },
    U() {
      // Seconds since UNIX epoch
      return (jsdate / 1000) | 0;
    },
  };

  const _date = function (format: string, timestamp: any) {
    jsdate =
      timestamp === undefined
        ? new Date() // Not provided
        : timestamp instanceof Date
        ? new Date(timestamp) // JS Date()
        : new Date(timestamp * 1000); // UNIX timestamp (auto-convert to int)
    return format.replace(formatChr, formatChrCb);
  };

  return _date(format, timestamp);
};

export const debounce = (fn: any, delay: number) => {
  let timer: any = null;
  return function () {
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const context = this,
      args = arguments;
    clearTimeout(timer);
    timer = setTimeout(function () {
      fn.apply(context, args);
    }, delay);
  };
};

export const sleep = (milliseconds: number) => {
  const start = new Date().getTime();
  for (let i = 0; i < 1e7; i++) {
    if (new Date().getTime() - start > milliseconds) {
      break;
    }
  }
};

export const throttle = (
  fn: any,
  threshhold: any = 250,
  scope: any = undefined
) => {
  threshhold || (threshhold = 250);
  let last: any, deferTimer: any;
  return function () {
    const context = scope || this;

    const now = +new Date(),
      args = arguments;
    if (last && now < last + threshhold) {
      // hold on to it
      clearTimeout(deferTimer);
      deferTimer = setTimeout(function () {
        last = now;
        fn.apply(context, args);
      }, threshhold);
    } else {
      last = now;
      fn.apply(context, args);
    }
  };
};

export const time = () => {
  return new Date().getTime();
};

export const getStampInMilliseconds = () => {
  return Date.now();
};

export const isoToDate = (str: string) => {
  const b = str.split(/\D/);
  // @ts-ignore
  return new Date(b[0], b[1] - 1, b[2], b[3], b[4], b[5]);
};

export const getOneYearInSeconds = () => {
  return ONE_YEAR_IN_SECONDS;
};

export const secondsToMilliseconds = (n: number) => {
  return n * 1000;
};
