import { EMAIL_REGEX } from '../constants';

export const isArray = (mixedVar: any): boolean => {
  if (typeof Array.isArray === 'undefined') {
    // @ts-ignore
    Array.isArray = function (obj) {
      return Object.prototype.toString.call(obj) === '[object Array]';
    };
  }

  return Array.isArray(mixedVar);
};

export const isFunction = (fn: any): boolean => {
  return typeof fn === 'function';
};

export const isNumeric = (value: any): boolean => {
  const v = String(value).replace(',', '.'),
    v1 = parseFloat(v);
  return !isNaN(v1) && isFinite(v1);
};

export const isString = (value: any): boolean => {
  return typeof value === 'string';
};

export const isObject = (mixedVar: any): boolean => {
  return typeof mixedVar === 'object';
};

export const isUndefined = (mixedVar: any): boolean => {
  return typeof mixedVar === 'undefined';
};

export const isDefined = (v: any): boolean => {
  return typeof v !== 'undefined' || v !== null;
};

export const isset = (): boolean => {
  // @ts-ignore
  const a = arguments,
    l = a.length;
  let i = 0,
    undef;

  if (l === 0) {
    throw new Error('Empty isset');
  }

  while (i !== l) {
    if (a[i] === undef || a[i] === null) {
      return false;
    }
    i++;
  }
  return true;
};

/**
 *
 * @param str
 */
export const isEmail = (str: string): boolean => {
  return new RegExp(EMAIL_REGEX).test(str);
};

/**
 *
 * @param value
 */
export const isDate = (value: any): boolean => {
  return (
    value !== null &&
    value !== undefined &&
    Object.prototype.toString.call(value) === '[object Date]' &&
    !isNaN(value.getTime())
  );
};

export function assertNever(value: never): never {
  throw new Error(
    `Unhandled discrimination union member: ${JSON.stringify(value)}`
  );
}
