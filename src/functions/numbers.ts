/**
 *
 * @param value
 * @param decimals
 * @param decimalPoint
 * @param thousandsSeparator
 */
export const formatNumber = (
  value: number,
  decimals: number,
  decimalPoint: string,
  thousandsSeparator: string
) => {
  let v = String(
    // @ts-ignore
    parseFloat(value)
      .toFixed(decimals)
      .replace(/\d(?=(\d{3})+\.)/g, '$&D')
  );
  v = String(v.replace('.', decimalPoint));
  v = v.replace(/D/g, thousandsSeparator);
  return v;
};

/**
 *
 * @param value
 */
export const cleanNumber = (value: string) => {
  return value.replace(/,/g, '').replace(/\./g, '').replace(/\D/g, '');
};

/**
 *
 * @param n
 * @param min
 * @param max
 */
export const ensureNumberBetween = (
  n: number,
  min: number | undefined = undefined,
  max: number | undefined = undefined
): number => {
  let r = Number(n);

  if (min && r < min) {
    r = min;
  } else if (max && r > max) {
    r = max;
  }

  return r;
};
