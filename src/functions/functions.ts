import { ILktObject } from '../interfaces/ILktObject';

/**
 *
 * @param that
 */
export function cloneFunction(that: unknown) {
  if (typeof that === 'function') {
    const temp: ILktObject = function temporary() {
      return that.apply(this, arguments);
    };
    for (const key in that) {
      // eslint-disable-next-line no-prototype-builtins
      if (that.hasOwnProperty(key)) {
        // @ts-ignore
        temp[key] = that[key];
      }
    }
    return temp;
  }
  return undefined;
}
