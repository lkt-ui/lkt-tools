export const supportsPromises = (): boolean => {
  return (
    typeof Promise !== 'undefined' &&
    Promise.toString().indexOf('[native code]') !== -1
  );
};

export const supportsLocalStorage = (): boolean => {
  let supportsLocalStorage = 1;
  if (typeof localStorage === 'object') {
    try {
      localStorage.setItem('localStorage', '1');
      localStorage.removeItem('localStorage');
    } catch (e) {
      supportsLocalStorage = -1;
    }
  } else {
    supportsLocalStorage = -1;
  }

  return supportsLocalStorage === 1;
};

export const supportsRgba = (): boolean => {
  const scriptElement = document.getElementsByTagName('script')[0],
    prevColor = scriptElement.style.color;
  try {
    scriptElement.style.color = 'rgba(1,5,13,0.44)';
    scriptElement.style.color = prevColor;
    return true;
  } catch (e) {
    return false;
  }
};

export const isAndroid = (): boolean => {
  return /(android)/i.test(navigator.userAgent);
};

export const isAndroid5 = (): boolean => {
  return /(android 5)/i.test(navigator.userAgent);
};

export const isSafari = (): boolean => {
  return /(safari)/i.test(navigator.userAgent) && !isChrome();
};

export const isChrome = (): boolean => {
  return /(chrome)/i.test(navigator.userAgent);
};

export const isIPhone = (): boolean => {
  return /(iPhone|iPod)/i.test(navigator.userAgent);
};

export const isIPad = (): boolean => {
  return /(iPad)/i.test(navigator.userAgent);
};

export const isMobile = (): boolean => {
  return /(mobile|iPhone|iPod)/i.test(navigator.userAgent);
};
