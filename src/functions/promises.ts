export const emptyPromise = (cb?: any) => {
  return new Promise(function (resolve, reject) {
    if (typeof cb === 'function') {
      cb();
    }
    resolve(undefined);
  });
};
