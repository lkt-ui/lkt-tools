export const DNI_LETTERS = 'TRWAGMYFPDXBNJZSQVHLCKE';
export const DNI_REGEX = /^(\d{8})([A-Z])$/;
export const CIF_REGEX = /^([ABCDEFGHJKLMNPQRSUVW])(\d{7})([0-9A-J])$/;
export const NIE_REGEX = /^[XYZ]\d{7,8}[A-Z]$/;
export const EMAIL_REGEX = '^[-!#$%&\'*+\\./0-9=?A-Z^_`a-z{|}~]+@[-!#$%&\'*+\\/0-9=?A-Z^_`a-z{|}~]+.[-!#$%&\'*+\\./0-9=?A-Z^_`a-z{|}~]+$';
export const ONE_YEAR_IN_SECONDS = 31536000;

export const DATE_TEXTS = [
    'Sun', 'Mon', 'Tues', 'Wednes', 'Thurs', 'Fri', 'Satur',
    'January', 'February', 'March', 'April', 'May', 'June',
    'July', 'August', 'September', 'October', 'November', 'December'
];