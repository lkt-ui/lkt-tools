import { openBlock as C, createElementBlock as D, createElementVNode as b } from "vue";
function X(e, t, n = !0) {
  let r = "";
  if (n) {
    for (r in t)
      if (t[r] === e)
        return !0;
  } else
    for (r in t)
      if (t[r] == e)
        return !0;
  return !1;
}
const V = (e, t) => {
  const n = [];
  for (let r = e; r < t; r++)
    n.push(r);
  return n;
};
function K(e, t) {
  return e.filter((n, r, o) => o.map((c) => c[t]).indexOf(n[t]) === r);
}
function Q(e, t) {
  const n = e.indexOf(t);
  return n >= 0 && e.splice(n, 1), e;
}
const q = (e, t) => e.map((n) => n[t]).map((n, r, o) => o.indexOf(n) === r && r).filter((n) => e[n]).map((n) => e[n]), ee = (e) => JSON.parse(JSON.stringify(e)), O = "TRWAGMYFPDXBNJZSQVHLCKE", v = /^(\d{8})([A-Z])$/, A = /^([ABCDEFGHJKLMNPQRSUVW])(\d{7})([0-9A-J])$/, x = /^[XYZ]\d{7,8}[A-Z]$/, P = "^[-!#$%&'*+\\./0-9=?A-Z^_`a-z{|}~]+@[-!#$%&'*+\\/0-9=?A-Z^_`a-z{|}~]+.[-!#$%&'*+\\./0-9=?A-Z^_`a-z{|}~]+$", F = 31536e3, y = [
  "Sun",
  "Mon",
  "Tues",
  "Wednes",
  "Thurs",
  "Fri",
  "Satur",
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December"
], te = (e) => (typeof Array.isArray > "u" && (Array.isArray = function(t) {
  return Object.prototype.toString.call(t) === "[object Array]";
}), Array.isArray(e)), T = (e) => typeof e == "function", ne = (e) => {
  const t = String(e).replace(",", "."), n = parseFloat(t);
  return !isNaN(n) && isFinite(n);
}, S = (e) => typeof e == "string", _ = (e) => typeof e == "object", l = (e) => typeof e > "u", re = (e) => typeof e < "u" || e !== null, oe = () => {
  const e = arguments, t = e.length;
  let n = 0, r;
  if (t === 0)
    throw new Error("Empty isset");
  for (; n !== t; ) {
    if (e[n] === r || e[n] === null)
      return !1;
    n++;
  }
  return !0;
}, ie = (e) => new RegExp(P).test(e), ae = (e) => e != null && Object.prototype.toString.call(e) === "[object Date]" && !isNaN(e.getTime());
function ce(e) {
  throw new Error(
    `Unhandled discrimination union member: ${JSON.stringify(e)}`
  );
}
const se = (e = "") => e.replace(/-([a-z])/g, function(t) {
  return t[1].toUpperCase();
}), ue = (e, t = "") => {
  let n, r = 0, o = 0;
  for (e += "", t ? (t += "", n = t.replace(/([[\]().?/*{}+$^:])/g, "$1")) : n = ` 
\r	\f\v \u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200A\u200B\u2028\u2029\u3000`, r = e.length, o = 0; r > o; o++)
    if (n.indexOf(e.charAt(o)) === -1) {
      e = e.substring(o);
      break;
    }
  for (r = e.length, o = r - 1; o >= 0; o--)
    if (n.indexOf(e.charAt(o)) === -1) {
      e = e.substring(0, o + 1);
      break;
    }
  return n.indexOf(e.charAt(0)) === -1 ? e : "";
}, le = (e) => (e += "", e.charAt(0).toUpperCase() + e.substring(1)), a = (e, t, n) => e.replace(new RegExp(t, "g"), n), N = (e, t) => e.replace(/\s/g, t), fe = (e, t) => {
  e === null && (e = ""), t = (`${t || ""}`.toLowerCase().match(/<[a-z][a-z0-9]*>/g) || []).join("");
  const n = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi, r = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
  return e.replace(r, "").replace(n, function(o, c) {
    return t.indexOf(`<${c.toLowerCase()}>`) > -1 ? o : "";
  });
}, de = (e, t, n, r) => {
  e = `${e}`.replace(/[^0-9+\-Ee.]/g, "");
  const o = isFinite(+e) ? +e : 0, c = isFinite(+t) ? Math.abs(t) : 0, p = typeof r > "u" ? "," : r, i = typeof n > "u" ? "." : n, u = function(f, g) {
    const h = Math.pow(10, g);
    return `${(Math.round(f * h) / h).toFixed(g)}`;
  };
  return s = (c ? u(o, c) : `${Math.round(o)}`).split("."), s[0].length > 3 && (s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, p)), (s[1] || "").length < c && (s[1] = s[1] || "", s[1] += new Array(c - s[1].length + 1).join("0")), s.join(i);
}, pe = (e) => {
  e = e.replace(/\r\n/g, `
`);
  let t = "";
  for (let n = 0; n < e.length; n++) {
    const r = e.charCodeAt(n);
    r < 128 ? t += String.fromCharCode(r) : r > 127 && r < 2048 ? (t += String.fromCharCode(r >> 6 | 192), t += String.fromCharCode(r & 63 | 128)) : (t += String.fromCharCode(r >> 12 | 224), t += String.fromCharCode(r >> 6 & 63 | 128), t += String.fromCharCode(r & 63 | 128));
  }
  return t;
}, ge = (e) => {
  let t = "", n = 0, r = 0, o = 0, c = 0;
  for (; n < e.length; )
    r = e.charCodeAt(n), r < 128 ? (t += String.fromCharCode(r), n++) : r > 191 && r < 224 ? (o = e.charCodeAt(n + 1), t += String.fromCharCode((r & 31) << 6 | o & 63), n += 2) : (o = e.charCodeAt(n + 1), c = e.charCodeAt(n + 2), t += String.fromCharCode(
      (r & 15) << 12 | (o & 63) << 6 | c & 63
    ), n += 3);
  return t;
}, he = (e = "", t = {}, n = ":", r = "") => {
  const o = [];
  for (const c in t)
    t.hasOwnProperty(c) && e.indexOf(n + c + r) > -1 && o.push(c);
  return o;
}, me = (e = "", t = {}, n = ":", r = "") => {
  for (const o in t)
    t.hasOwnProperty(o) && (e = e.replace(n + o + r, t[o]));
  return e;
}, we = (e = 10) => {
  let t = "";
  const n = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789", r = n.length;
  for (let o = 0; o < e; o++)
    t += n.charAt(Math.floor(Math.random() * r));
  return t;
}, m = (e) => String(e).replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;"), w = (e) => String(e), ye = (e) => (S(e) || (e = w(e)), !!e), be = (e) => ` ${e}`.slice(1);
function d(e, t, n) {
  for (e = String(e); e.length < t; )
    e = n + e;
  return e;
}
const Ae = () => window.Blob || window.MozBlob || window.WebKitBlob || w(), U = (e) => {
  for (let t = 0; t < e.childNodes.length; t++) {
    const n = e.childNodes[t];
    n.nodeType === 8 || n.nodeType === 3 && !/\S/.test(n.nodeValue) ? (e.removeChild(n), t--) : n.nodeType === 1 && U(n);
  }
  return !0;
}, Se = (e) => {
  e = String(e);
  let t = "/";
  e.indexOf("C:\\") === 0 && (t = "\\");
  let n = e.substring(e.lastIndexOf(t) + 1);
  return n.lastIndexOf(".") !== -1 && (n = n.substring(0, n.lastIndexOf("."))), n;
};
function Ee(e) {
  if (typeof e == "function") {
    const t = function() {
      return e.apply(this, arguments);
    };
    for (const n in e)
      e.hasOwnProperty(n) && (t[n] = e[n]);
    return t;
  }
}
const Ce = (e = "") => S(e) && e !== "" && e.indexOf("data:image/") !== -1, De = () => typeof Promise < "u" && Promise.toString().indexOf("[native code]") !== -1, Oe = () => {
  let e = 1;
  if (typeof localStorage == "object")
    try {
      localStorage.setItem("localStorage", "1"), localStorage.removeItem("localStorage");
    } catch {
      e = -1;
    }
  else
    e = -1;
  return e === 1;
}, ve = () => {
  const e = document.getElementsByTagName("script")[0], t = e.style.color;
  try {
    return e.style.color = "rgba(1,5,13,0.44)", e.style.color = t, !0;
  } catch {
    return !1;
  }
}, xe = () => /(android)/i.test(navigator.userAgent), Pe = () => /(android 5)/i.test(navigator.userAgent), Fe = () => /(safari)/i.test(navigator.userAgent) && !k(), k = () => /(chrome)/i.test(navigator.userAgent), Te = () => /(iPhone|iPod)/i.test(navigator.userAgent), _e = () => /(iPad)/i.test(navigator.userAgent), Ne = () => /(mobile|iPhone|iPod)/i.test(navigator.userAgent), Ue = (e, t, n, r) => {
  let o = String(
    parseFloat(e).toFixed(t).replace(/\d(?=(\d{3})+\.)/g, "$&D")
  );
  return o = String(o.replace(".", n)), o = o.replace(/D/g, r), o;
}, ke = (e) => e.replace(/,/g, "").replace(/\./g, "").replace(/\D/g, ""), Me = (e, t = void 0, n = void 0) => {
  let r = Number(e);
  return t && r < t ? r = t : n && r > n && (r = n), r;
}, Ie = (e) => typeof e == "object" ? Object.assign(Object.create(Object.getPrototypeOf(e)), e) : {}, M = (e) => {
  const t = [];
  for (const r in e) {
    let o = e[r];
    _(o) && (o = M(o)), t.push([r, o]);
  }
  t.sort(function(r, o) {
    return r[0] - o[0];
  });
  const n = {};
  return t.forEach((r) => {
    n[r[0]] = r[1];
  }), n;
}, $e = (e, t) => {
  const n = t.split("."), r = n.length;
  let o = 0, c = e;
  for (; !l(c[n[o]]); )
    c = c[n[o]], ++o;
  if (!(o < r))
    return c;
}, Le = (e, t) => {
  for (const n in t)
    t.hasOwnProperty(n) && (e[n] = t[n]);
  return e;
}, Ye = (e, t) => {
  for (const n in t)
    e.hasOwnProperty(n) && t.hasOwnProperty(n) && (e[n] = t[n]);
  return e;
}, je = (e, t) => (t.forEach((n) => {
  e.hasOwnProperty(n) && delete e[n];
}), e), Re = (e) => Object.keys(e).map(function(t) {
  return e[t];
}), Be = (e) => new Promise(function(t, n) {
  T(e) && e(), t(void 0);
}), ze = (e = void 0, t = void 0, n = void 0) => {
  const r = new URL(`mailto:${e}`);
  return l(t) || r.searchParams.append("subject", t), l(n) || r.searchParams.append("body", n), r.href;
}, Ge = (e = void 0) => {
  l(e) && (e = window.location.href);
  const t = new URL("https://www.facebook.com/sharer/sharer.php");
  return t.searchParams.append("u", e), t.href;
}, He = (e = void 0, t = void 0, n = []) => {
  l(e) && (e = window.location.href);
  const r = new URL("https://twitter.com/intent/tweet");
  return r.searchParams.append("url", e), l(t) || r.searchParams.append("via", t), n.length > 0 && r.searchParams.append("hashtags", n.join(" ")), r.href;
}, We = (e = void 0) => {
  l(e) && (e = window.location.href);
  const t = new URL("https://plus.google.com/share");
  return t.searchParams.append("url", e), t.href;
}, Je = (e = void 0, t = void 0, n = void 0, r = void 0) => {
  l(e) && (e = window.location.href);
  const o = new URL("https://www.linkedin.com/shareArticle");
  return o.searchParams.append("mini", "true"), o.searchParams.append("url", m(e)), l(t) || o.searchParams.append("title", m(t)), l(n) || o.searchParams.append("summary", m(n)), l(r) || o.searchParams.append("source", m(r)), o.href;
}, Ze = (e = void 0, t = void 0, n = void 0) => {
  l(e) && (e = window.location.href);
  const r = new URL("//es.pinterest.com/pin/create/button/");
  return r.searchParams.append("url", e), l(t) || r.searchParams.append("media", t), l(n) || r.searchParams.append("description", n), r.href;
}, Xe = (e = void 0, t = "") => {
  l(e) && (e = window.location.href), t = `${m(t)} ${e}`;
  let n = new URL("whatsapp://send");
  return n.searchParams.append("text", t), n.href;
}, I = (e) => {
  e = e.toUpperCase().replace(/\s/, "");
  let t = !1;
  const n = $(e);
  switch (n) {
    case "dni":
      t = E(e);
      break;
    case "nie":
      t = L(e);
      break;
    case "cif":
      t = Y(e);
      break;
  }
  return {
    type: n,
    valid: t
  };
}, $ = (e) => {
  if (e.match(v))
    return "dni";
  if (e.match(A))
    return "cif";
  if (e.match(x))
    return "nie";
}, E = (e) => e.charAt(8) === O.charAt(parseInt(e, 10) % 23), L = (e) => {
  const t = e.charAt(0);
  let n = 0;
  switch (t) {
    case "X":
      n = 0;
      break;
    case "Y":
      n = 1;
      break;
    case "Z":
      n = 2;
      break;
  }
  return E(n + e.substring(1));
}, Y = (e) => {
  const t = e.match(A);
  if (!t)
    return !1;
  const n = t[1], r = t[2], o = t[3];
  let c = 0, p = 0, i;
  for (let h = 0; h < r.length; h++)
    i = parseInt(r[h], 10), h % 2 === 0 ? (i *= 2, p += i < 10 ? i : i - 9) : c += i;
  const u = parseFloat((c + p).toString().substring(-1)), f = 10 - u, g = "JABCDEFGHI".substring(f, 1);
  return n.match(/[ABEH]/) ? o == f : n.match(/[KPQS]/) ? o == g : o == f || o == g;
}, Ve = (e) => I(e), Ke = (e, t = void 0) => {
  let n, r = {};
  const o = /\\?(.?)/gi, c = function(i, u) {
    return r[i] ? r[i]() : u;
  };
  return r = {
    d() {
      return d(r.j(), 2, "0");
    },
    D() {
      return r.l().slice(0, 3);
    },
    j() {
      return n.getDate();
    },
    l() {
      return `${y[r.w()]}day`;
    },
    N() {
      return r.w() || 7;
    },
    S() {
      const i = r.j();
      let u = i % 10;
      return u <= 3 && parseInt(String(i % 100 / 10), 10) === 1 && (u = 0), ["st", "nd", "rd"][u - 1] || "th";
    },
    w() {
      return n.getDay();
    },
    z() {
      const i = new Date(r.Y(), r.n() - 1, r.j()), u = new Date(r.Y(), 0, 1);
      return Math.round((i - u) / 864e5);
    },
    W() {
      const i = new Date(r.Y(), r.n() - 1, r.j() - r.N() + 3), u = new Date(i.getFullYear(), 0, 4);
      return d(1 + Math.round((i - u) / 864e5 / 7), 2, "0");
    },
    F() {
      return y[6 + r.n()];
    },
    m() {
      return d(r.n(), 2, "0");
    },
    M() {
      return r.F().slice(0, 3);
    },
    n() {
      return n.getMonth() + 1;
    },
    t() {
      return new Date(r.Y(), r.n(), 0).getDate();
    },
    L() {
      const i = r.Y();
      return i % 4 === 0 & i % 100 !== 0 | i % 400 === 0;
    },
    o() {
      const i = r.n(), u = r.W();
      return r.Y() + (i === 12 && u < 9 ? 1 : i === 1 && u > 9 ? -1 : 0);
    },
    Y() {
      return n.getFullYear();
    },
    y() {
      return r.Y().toString().slice(-2);
    },
    a() {
      return n.getHours() > 11 ? "pm" : "am";
    },
    A() {
      return r.a().toUpperCase();
    },
    B() {
      const i = n.getUTCHours() * 3600, u = n.getUTCMinutes() * 60, f = n.getUTCSeconds();
      return d(Math.floor((i + u + f + 3600) / 86.4) % 1e3, 3, "0");
    },
    g() {
      return r.G() % 12 || 12;
    },
    G() {
      return n.getHours();
    },
    h() {
      return d(r.g(), 2, "0");
    },
    H() {
      return d(r.G(), 2, "0");
    },
    i() {
      return d(n.getMinutes(), 2, "0");
    },
    s() {
      return d(n.getSeconds(), 2, "0");
    },
    u() {
      return d(n.getMilliseconds() * 1e3, 6, "0");
    },
    e() {
      const i = "Not supported (see source code of date() for timezone on how to add support)";
      throw new Error(i);
    },
    I() {
      const i = new Date(r.Y(), 0), u = Date.UTC(r.Y(), 0), f = new Date(r.Y(), 6), g = Date.UTC(r.Y(), 6);
      return i - u !== f - g ? 1 : 0;
    },
    O() {
      const i = n.getTimezoneOffset(), u = Math.abs(i);
      return (i > 0 ? "-" : "+") + d(Math.floor(u / 60) * 100 + u % 60, 4, "0");
    },
    P() {
      const i = r.O();
      return `${i.substr(0, 3)}:${i.substr(3, 2)}`;
    },
    T() {
      return "UTC";
    },
    Z() {
      return -n.getTimezoneOffset() * 60;
    },
    c() {
      return "Y-m-d\\TH:i:sP".replace(o, c);
    },
    r() {
      return "D, d M Y H:i:s O".replace(o, c);
    },
    U() {
      return n / 1e3 | 0;
    }
  }, function(i, u) {
    return n = u === void 0 ? new Date() : u instanceof Date ? new Date(u) : new Date(u * 1e3), i.replace(o, c);
  }(e, t);
}, Qe = (e, t) => {
  let n = null;
  return function() {
    const r = this, o = arguments;
    clearTimeout(n), n = setTimeout(function() {
      e.apply(r, o);
    }, t);
  };
}, qe = (e) => {
  const t = new Date().getTime();
  for (let n = 0; n < 1e7 && !(new Date().getTime() - t > e); n++)
    ;
}, et = (e, t = 250, n = void 0) => {
  t || (t = 250);
  let r, o;
  return function() {
    const c = n || this, p = +new Date(), i = arguments;
    r && p < r + t ? (clearTimeout(o), o = setTimeout(function() {
      r = p, e.apply(c, i);
    }, t)) : (r = p, e.apply(c, i));
  };
}, tt = () => new Date().getTime(), nt = () => Date.now(), rt = (e) => {
  const t = e.split(/\D/);
  return new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);
}, ot = () => F, it = (e) => e * 1e3, at = (e) => decodeURIComponent(
  w(e).replace(/%(?![\da-f]{2})/gi, "%25").replace(/\+/g, "%20")
), ct = (e) => (e = e.toLowerCase(), e = N(e, "-"), e = a(e, "\xF1", "n"), e = a(e, "'", ""), e = a(e, "\xB4", ""), e = a(e, "\\", ""), e = a(e, ".", ""), e = a(e, ",", ""), e = a(e, "/", ""), e = a(e, ":", ""), e = a(e, "\xE1", "a"), e = a(e, "\xE9", "e"), e = a(e, "\xED", "i"), e = a(e, "\xF3", "o"), e = a(e, "\xFA", "u"), e = a(e, "\xE0", "a"), e = a(e, "\xE0", "e"), e = a(e, "\xE0", "i"), e = a(e, "\xE0", "o"), e = a(e, "\xE0", "u"), e = a(e, "\u1E83", "w"), e = a(e, "\u1E81", "w"), e = a(e, "\xFD", "y"), e = a(e, "\u1EF3", "y"), e = a(e, "\u015B", "s"), e = a(e, "\u01F5", "g"), e = a(e, "\u1E31", "k"), e = a(e, "\u013A", "l"), e = a(e, "\u017A", "z"), e = a(e, "\u0107", "c"), e = a(e, "\u01D8", "v"), e = a(e, "\u0144", "n"), e = a(e, "\u1E3F", "m"), e = a(e, "\u01F9", "n"), e = a(e, "\u01DC", "v"), e = a(e, "\xE4", "a"), e = a(e, "\xEB", "e"), e = a(e, "\xEF", "i"), e = a(e, "\xF6", "o"), e = a(e, "\xFC", "u"), e = a(e, "\u1E85", "w"), e = a(e, "\xFF", "y"), e = a(e, "\u1E8D", "x"), e), st = (e, t) => {
  const n = {};
  let r = {};
  if (e && (r = Object.assign(r, e)), t) {
    for (const o in r)
      o.indexOf(t) === 0 && (n[o.replace(t, "")] = r[o]);
    return n;
  }
  for (const o in r)
    n[o] = r[o];
  return n;
}, ut = (e, t) => !!e.$slots[t], j = {
  name: "LktDisableWebkitAutoFill"
}, R = (e, t) => {
  const n = e.__vccOpts || e;
  for (const [r, o] of t)
    n[r] = o;
  return n;
}, B = { style: { width: "0", height: "0", overflow: "hidden", position: "fixed", left: "0", top: "0", "z-index": "-1" } }, z = /* @__PURE__ */ b("input", { type: "email" }, null, -1), G = /* @__PURE__ */ b("input", { type: "password" }, null, -1), H = [
  z,
  G
];
function W(e, t, n, r, o, c) {
  return C(), D("div", B, H);
}
const J = /* @__PURE__ */ R(j, [["render", W]]), lt = {
  install: (e, t) => {
    e.component("lkt-disable-webkit-autofill", J);
  }
};
export {
  ce as assertNever,
  ke as cleanNumber,
  ee as cloneArray,
  Ee as cloneFunction,
  Ie as cloneObject,
  be as cloneString,
  V as createArrayFromRange,
  Ke as date,
  Qe as debounce,
  at as decodeUrl,
  lt as default,
  je as deleteObjectKeys,
  Be as emptyPromise,
  Me as ensureNumberBetween,
  he as extractFillData,
  $e as fetchInObject,
  me as fill,
  Ue as formatNumber,
  we as generateRandomString,
  q as getArrayUniqueKeyValues,
  Se as getBaseName,
  Ae as getBlobConverter,
  Ge as getFacebookUrl,
  We as getGooglePlusUrl,
  Je as getLinkedInUrl,
  ze as getMailUrl,
  ot as getOneYearInSeconds,
  Ze as getPinterestUrl,
  st as getSlots,
  nt as getStampInMilliseconds,
  He as getTwitterUrl,
  ct as getUrlSlug,
  Xe as getWhatsAppUrl,
  m as htmlEntities,
  X as inArray,
  xe as isAndroid,
  Pe as isAndroid5,
  te as isArray,
  Ce as isBase64String,
  k as isChrome,
  ae as isDate,
  re as isDefined,
  ie as isEmail,
  ye as isFilled,
  T as isFunction,
  _e as isIPad,
  Te as isIPhone,
  Ne as isMobile,
  ne as isNumeric,
  _ as isObject,
  Fe as isSafari,
  S as isString,
  l as isUndefined,
  Ve as isValidSpanishID,
  rt as isoToDate,
  oe as isset,
  se as kebabCaseToCamelCase,
  Le as mergeObjects,
  de as numberFormat,
  Re as objectToArray,
  a as replaceAll,
  N as replaceSingleWhiteSpaces,
  K as rmArrayDuplicates,
  Q as rmArrayValue,
  U as rmEmptyDOMNodes,
  Ye as safeMergeObjects,
  it as secondsToMilliseconds,
  qe as sleep,
  ut as slotProvided,
  M as sortObject,
  fe as stripTags,
  Oe as supportsLocalStorage,
  De as supportsPromises,
  ve as supportsRgba,
  et as throttle,
  tt as time,
  w as toString,
  ue as trim,
  le as ucfirst,
  ge as utf8Decode,
  pe as utf8Encode
};
