export declare const DNI_LETTERS = "TRWAGMYFPDXBNJZSQVHLCKE";
export declare const DNI_REGEX: RegExp;
export declare const CIF_REGEX: RegExp;
export declare const NIE_REGEX: RegExp;
export declare const EMAIL_REGEX = "^[-!#$%&'*+\\./0-9=?A-Z^_`a-z{|}~]+@[-!#$%&'*+\\/0-9=?A-Z^_`a-z{|}~]+.[-!#$%&'*+\\./0-9=?A-Z^_`a-z{|}~]+$";
export declare const ONE_YEAR_IN_SECONDS = 31536000;
export declare const DATE_TEXTS: string[];
