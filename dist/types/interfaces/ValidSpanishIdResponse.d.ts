import { SpanishIdType } from '../types/Types';
export interface ValidSpanishIdResponse {
    valid: boolean;
    type: SpanishIdType | undefined;
}
