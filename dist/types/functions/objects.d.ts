import { ILktObject } from '../interfaces/ILktObject';
export declare const cloneObject: (obj: object) => any;
export declare const sortObject: (obj: ILktObject) => ILktObject;
export declare const fetchInObject: (obj: object, key: string) => object | undefined;
export declare const mergeObjects: (obj1: object, obj2: object) => object;
export declare const safeMergeObjects: (obj1: object, obj2: object) => object;
export declare const deleteObjectKeys: (obj: object, keys: string[]) => object;
export declare const objectToArray: (t: object) => any[];
