import { ILktObject } from '../interfaces/ILktObject';
export declare const getSlots: ($slots: object, slotStartingPattern?: string) => ILktObject;
export declare const slotProvided: ($vm: any, name: string) => boolean;
