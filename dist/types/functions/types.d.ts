export declare const isArray: (mixedVar: any) => boolean;
export declare const isFunction: (fn: any) => boolean;
export declare const isNumeric: (value: any) => boolean;
export declare const isString: (value: any) => boolean;
export declare const isObject: (mixedVar: any) => boolean;
export declare const isUndefined: (mixedVar: any) => boolean;
export declare const isDefined: (v: any) => boolean;
export declare const isset: () => boolean;
/**
 *
 * @param str
 */
export declare const isEmail: (str: string) => boolean;
/**
 *
 * @param value
 */
export declare const isDate: (value: any) => boolean;
export declare function assertNever(value: never): never;
