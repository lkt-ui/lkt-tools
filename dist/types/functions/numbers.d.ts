/**
 *
 * @param value
 * @param decimals
 * @param decimalPoint
 * @param thousandsSeparator
 */
export declare const formatNumber: (value: number, decimals: number, decimalPoint: string, thousandsSeparator: string) => string;
/**
 *
 * @param value
 */
export declare const cleanNumber: (value: string) => string;
/**
 *
 * @param n
 * @param min
 * @param max
 */
export declare const ensureNumberBetween: (n: number, min?: number | undefined, max?: number | undefined) => number;
