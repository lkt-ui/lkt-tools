/**
 *
 * @param needle
 * @param haystack
 * @param strict
 */
export declare function inArray<T>(needle: unknown, haystack: T[], strict?: boolean): boolean;
/**
 *
 * @param start
 * @param end
 */
export declare const createArrayFromRange: (start: number, end: number) => number[];
/**
 *
 * @param myArr
 * @param prop
 */
export declare function rmArrayDuplicates<T>(myArr: T[], prop: any): T[];
/**
 *
 * @param myArr
 * @param value
 */
export declare function rmArrayValue<T>(myArr: T[], value: any): T[];
/**
 *
 * @param arr
 * @param prop
 */
export declare const getArrayUniqueKeyValues: (arr: any[], prop: any) => any[];
/**
 *
 * @param arr
 */
export declare const cloneArray: (arr: any[]) => any[];
