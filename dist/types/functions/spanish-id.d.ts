import { ValidSpanishIdResponse } from '../interfaces/ValidSpanishIdResponse';
import { SpanishIdType } from '../types/Types';
export declare const validateSpanishID: (str: string) => {
    type: SpanishIdType | undefined;
    valid: boolean;
};
export declare const getSpanishIdType: (str: string) => SpanishIdType | undefined;
export declare const validDNI: (dni: string) => boolean;
export declare const validNIE: (nie: string) => boolean;
export declare const validCIF: (cif: string) => boolean;
export declare const isValidSpanishID: (value: string) => ValidSpanishIdResponse;
