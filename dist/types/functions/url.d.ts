export declare const decodeUrl: (str: string) => string;
export declare const getUrlSlug: (str: string) => string;
