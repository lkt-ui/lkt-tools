import { ILktObject } from '../interfaces/ILktObject';
/**
 *
 * @param that
 */
export declare function cloneFunction(that: unknown): ILktObject | undefined;
