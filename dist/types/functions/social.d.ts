export declare const getMailUrl: (email?: string, subject?: string, body?: string) => string;
export declare const getFacebookUrl: (url?: string) => string;
export declare const getTwitterUrl: (url?: string, via?: string, hashtags?: any[]) => string;
export declare const getGooglePlusUrl: (url?: string) => string;
export declare const getLinkedInUrl: (url?: string, title?: string, summary?: string, source?: string) => string;
export declare const getPinterestUrl: (url?: string, media?: string, description?: string) => string;
export declare const getWhatsAppUrl: (url?: string, text?: string) => string;
