export declare const date: (format: string, timestamp?: any) => string;
export declare const debounce: (fn: any, delay: number) => () => void;
export declare const sleep: (milliseconds: number) => void;
export declare const throttle: (fn: any, threshhold?: any, scope?: any) => () => void;
export declare const time: () => number;
export declare const getStampInMilliseconds: () => number;
export declare const isoToDate: (str: string) => Date;
export declare const getOneYearInSeconds: () => number;
export declare const secondsToMilliseconds: (n: number) => number;
